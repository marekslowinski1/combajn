package com.marslow.combajn.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import javax.servlet.http.HttpServletResponse;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtTokenUtil jwtTokenUtil;

    public SecurityConfig(JwtTokenUtil jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
            .antMatchers("/h2/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Enable CORS and disable CSRF
        http = http.cors().and().csrf().disable();
        // Set session management to stateless
        http = http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and();

        // Set unauthorized requests exception handler
        http = http
                .exceptionHandling()
                .authenticationEntryPoint(
                        (request, response, ex) -> {
                            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
                        }
                )
                .and();

        // Set permissions on endpoints
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/getToken").permitAll()
                // Our public endpoints
                .antMatchers(HttpMethod.GET, "/api/product").permitAll()
                .antMatchers(HttpMethod.GET, "/api/product/all").permitAll()
                .antMatchers(HttpMethod.GET, "/api/order/all").permitAll()
                .antMatchers(HttpMethod.POST, "/api/order").permitAll()
                .antMatchers("/api/customer").hasRole("CUSTOMER")
                .antMatchers("/api/customer/all").hasRole("CUSTOMER")
                .antMatchers("/api/admin/customer").hasRole("ADMIN")
                .antMatchers("/api/admin/order").hasRole("ADMIN")
                .antMatchers("/api/admin/product").hasRole("ADMIN")
                .and().addFilter(new JwtTokenFilter(authenticationManager(), jwtTokenUtil));

    }

    // Expose authentication manager bean
    @Override @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

}