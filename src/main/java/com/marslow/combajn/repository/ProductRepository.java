package com.marslow.combajn.repository;

import com.marslow.combajn.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {}
