package com.marslow.combajn.repository;

import com.marslow.combajn.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {}
