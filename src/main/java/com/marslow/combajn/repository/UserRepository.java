package com.marslow.combajn.repository;


import com.marslow.combajn.entity.UserDto;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserDto, Long> {
    Optional<UserDto> getUserDtoByName(String name);
}
