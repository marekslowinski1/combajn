package com.marslow.combajn.repository;

import com.marslow.combajn.entity.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {}
