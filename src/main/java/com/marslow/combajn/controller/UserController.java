package com.marslow.combajn.controller;

import com.marslow.combajn.config.security.JwtTokenUtil;
import com.marslow.combajn.model.User;
import com.marslow.combajn.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class UserController {
    private final UserService userService;
    private final JwtTokenUtil jwtTokenUtil;

    @PostMapping(value = "/api/getToken", consumes = "application/json")
    private ResponseEntity<String> getToken(@RequestBody User request) {

        boolean authenticated = userService.credentialsMatch(request);
        if (!authenticated) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        User user = userService.getUserByName(request.getName());
        String token = jwtTokenUtil.generateAccessToken(user);
        return ResponseEntity.ok()
                .header(HttpHeaders.AUTHORIZATION, token).body(token);
    }


}
