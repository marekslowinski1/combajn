package com.marslow.combajn.controller;

import com.marslow.combajn.entity.Order;
import com.marslow.combajn.service.OrderService;
import com.marslow.combajn.util.PatchHelper;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.json.JsonMergePatch;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class OrderController {
    private final OrderService orderService;
    private final PatchHelper patchHelper;

    @GetMapping("/api/order/all")
    private List<Order> getAllOrders() {
        return  orderService.getAllOrders();
    }

    @GetMapping("/api/order")
    private Order getOrder(@RequestParam Long id) {
        return orderService.getOrder(id).get();
    }

    @PostMapping("/api/admin/order")
    private Long saveOrder(@RequestBody Order order) {
        orderService.saveOrUpdateOrder(order);
        return order.getId();
    }

    @PutMapping("/api/admin/order")
    private void updateOrder(@RequestParam Long id, @RequestBody Order order) {
        order.setId(id);
        orderService.saveOrUpdateOrder(order);
    }

    @PatchMapping(path = "/api/admin/order")
    public void updateOrder(@RequestParam Long id,
                               @RequestBody JsonMergePatch mergePatchDocument) {

        Order order = orderService.getOrder(id).orElseThrow();
        Order orderPatched = patchHelper.mergePatch(mergePatchDocument, order, Order.class);
        orderService.saveOrUpdateOrder(orderPatched);
    }
}
