package com.marslow.combajn.controller;

import com.marslow.combajn.entity.Customer;
import com.marslow.combajn.service.CustomerService;
import com.marslow.combajn.util.PatchHelper;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.json.JsonMergePatch;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class CustomerController {
    private final CustomerService customerService;
    private final PatchHelper patchHelper;

    @GetMapping("/api/customer/all")
    private List<Customer> getAllCustomers() {
        return  customerService.getAllCustomers();
    }

    @GetMapping("/api/customer")
    private Customer getCustomer(@RequestParam Long id) {
        return customerService.getCustomer(id).get();
    }

    @PostMapping("/api/admin/customer")
    private Long saveCustomer(@RequestBody Customer customer) {
        customerService.saveOrUpdateCustomer(customer);
        return customer.getId();
    }

    @PutMapping("/api/admin/customer")
    private void updateCustomer(@RequestParam Long id, @RequestBody Customer customer) {
        customer.setId(id);
        customerService.saveOrUpdateCustomer(customer);
    }

    @PatchMapping(path = "/api/admin/customer")
    public void updateCustomer(@RequestParam Long id,
                              @RequestBody JsonMergePatch mergePatchDocument) {

        Customer customer = customerService.getCustomer(id).orElseThrow();
        Customer customerPatched = patchHelper.mergePatch(mergePatchDocument, customer, Customer.class);
        customerService.saveOrUpdateCustomer(customerPatched);
    }
}
