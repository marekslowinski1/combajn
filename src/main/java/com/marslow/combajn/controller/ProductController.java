package com.marslow.combajn.controller;

import com.marslow.combajn.entity.Product;
import com.marslow.combajn.service.ProductService;
import com.marslow.combajn.util.PatchHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.json.JsonMergePatch;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class ProductController {
    private final ProductService productService;
    private final PatchHelper patchHelper;

    @GetMapping("/api/product/all")
    private List<Product> getAllProducts() {
        return  productService.getAllProducts();
    }

    @GetMapping("/api/product")
    private Product getProduct(@RequestParam Long id) {
        return productService.getProduct(id).get();
    }

    @PostMapping("/api/admin/product")
    private Long saveProduct(@RequestBody Product product) {
        productService.saveOrUpdateProduct(product);
        return product.getId();
    }

    @PutMapping("/api/admin/product")
    private void updateProduct(@RequestParam Long id, @RequestBody Product product) {
        product.setId(id);
        productService.saveOrUpdateProduct(product);
    }

    @PatchMapping(path = "/api/admin/product")
    public void updateProduct(@RequestParam Long id,
                                              @RequestBody JsonMergePatch mergePatchDocument) {

        Product product = productService.getProduct(id).orElseThrow();
        Product productPatched = patchHelper.mergePatch(mergePatchDocument, product, Product.class);
        productService.saveOrUpdateProduct(productPatched);
    }

}
