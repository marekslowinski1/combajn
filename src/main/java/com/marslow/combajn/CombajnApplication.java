package com.marslow.combajn;

import com.marslow.combajn.entity.Customer;
import com.marslow.combajn.entity.Order;
import com.marslow.combajn.entity.Product;
import com.marslow.combajn.model.User;
import com.marslow.combajn.repository.CustomerRepository;
import com.marslow.combajn.repository.OrderRepository;
import com.marslow.combajn.repository.ProductRepository;
import com.marslow.combajn.service.UserService;
import lombok.val;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class CombajnApplication {

    public static void main(String[] args) {
        SpringApplication.run(CombajnApplication.class, args);
    }

    @Bean
    public ApplicationRunner initializer(ProductRepository productRepository, CustomerRepository customerRepository, OrderRepository orderRepository, UserService userService) {
        return args -> {
            val product1 = Product.builder().name("Motor").price(150.40f).available(true).build();
            val product2 = Product.builder().name("Vase").price(10.90f).available(false).build();
            val product3 = Product.builder().name("Piezoceramic").price(550.00f).available(true).build();
            val product4 = Product.builder().name("Ball").price(23.45f).available(false).build();

            val customer1 = Customer.builder().name("Patryk").address("Bosaka 22/7, 22-320 Poznań").build();
            val customer2 = Customer.builder().name("Andrzej").address("Jozuego 27, 45-320 Kraków").build();
            val customer3 = Customer.builder().name("Tomasz").address("Miła 22, 77-770 Wrocław").build();
            val customer4 = Customer.builder().name("Janusz").address("Przyjemna 13/2, 12-220 Łódź").build();

            Set<Product> products1 = new HashSet() {
                {
                    add(product1);
                    add(product2);
                }};

            Set<Product> products2 = new HashSet() {
                {
                    add(product2);
                    add(product3);
                    add(product4);
                }};

            val order1 = Order.builder().placeDate(LocalDateTime.now()).status("in progress").customer(customer1).products(products1).build();
            val order2 = Order.builder().placeDate(LocalDateTime.now()).status("in progress").customer(customer2).products(products2).build();

            val user_rolecustomer = User.builder().name("Adam").role("ROLE_CUSTOMER").password("alamakota").build();
            val user_roleadmin = User.builder().name("Piotr").role("ROLE_ADMIN").password("dwaplusdwa").build();

            productRepository.saveAll(Arrays.asList(product1, product2, product3, product4));
            customerRepository.saveAll(Arrays.asList(customer1, customer2, customer3, customer4));
            orderRepository.saveAll(Arrays.asList(order1, order2));
            userService.saveAll(Arrays.asList(user_roleadmin, user_rolecustomer));
        };
    }

}
