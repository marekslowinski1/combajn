package com.marslow.combajn.service;

import com.marslow.combajn.entity.UserDto;
import com.marslow.combajn.model.User;
import com.marslow.combajn.repository.UserRepository;
import lombok.Data;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Data
@Service
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public void saveAll(Iterable<User> users){
        userRepository.saveAll(map(users));
    }

    public UserDto getUserDtoByName(String name) {
        return userRepository.getUserDtoByName(name).orElseThrow(
                () -> new UsernameNotFoundException(String.format("User with name - %s, not found", name))
        );
    }

    public User getUserByName(String name) {
        UserDto userDto = userRepository.getUserDtoByName(name).orElseThrow(
                () -> new UsernameNotFoundException(String.format("User with name - %s, not found", name))
        );
        return userDtoToUser(userDto);
    }

    public boolean credentialsMatch(User user) {
        UserDto userDto = getUserDtoByName(user.getName());
        return user.getName().equals(userDto.getName())
                && passwordEncoder.matches(user.getPassword(), userDto.getPasswordHash());
    }

    private User userDtoToUser(UserDto userDto) {
        User user = User.builder()
                .name(userDto.getName())
                .role(userDto.getRole()).build();
        return user;
    }

    private UserDto userToUserDto(User user) {
        UserDto userDto = UserDto.builder()
                .name(user.getName())
                .role(user.getRole())
                .passwordHash(passwordEncoder.encode(user.getPassword())).build();
        return userDto;
    }

    private Iterable<UserDto> map(Iterable<User> users) {
        if (users == null) {
            return null;
        }

        List<UserDto> list = new ArrayList<>();
        for (User user : users) {
            list.add(userToUserDto(user));
        }

        return list;
    }
}
