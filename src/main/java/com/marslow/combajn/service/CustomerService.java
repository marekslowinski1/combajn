package com.marslow.combajn.service;

import com.marslow.combajn.entity.Customer;
import com.marslow.combajn.repository.CustomerRepository;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@Service
public class CustomerService {
    final CustomerRepository customerRepository;

    public List<Customer> getAllCustomers() {
        List<Customer> customers = new ArrayList<>();
        customerRepository.findAll().forEach(customers::add);
        return customers;
    }

    public Optional<Customer> getCustomer(Long id) {
        return customerRepository.findById(id);
    }

    public void saveOrUpdateCustomer(Customer customer) {
        customerRepository.save(customer);
    }
}
