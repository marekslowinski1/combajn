package com.marslow.combajn.service;

import com.marslow.combajn.entity.Order;
import com.marslow.combajn.repository.OrderRepository;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@Service
public class OrderService {
    final OrderRepository orderRepository;

    public List<Order> getAllOrders() {
        List<Order> orders = new ArrayList<>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }

    public Optional<Order> getOrder(Long id) {
        return orderRepository.findById(id);
    }

    public void saveOrUpdateOrder(Order order) {
        orderRepository.save(order);
    }
}
