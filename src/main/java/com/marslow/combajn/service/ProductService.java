package com.marslow.combajn.service;

import com.marslow.combajn.entity.Product;
import com.marslow.combajn.repository.ProductRepository;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@Service
public class ProductService {
    final ProductRepository productRepository;

    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add);
        return products;
    }

    public Optional<Product> getProduct(Long id) {
        return productRepository.findById(id);
    }

    public void saveOrUpdateProduct(Product product) {
        productRepository.save(product);
    }
}
